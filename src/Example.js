import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Example = () => {
    const [dataTampil, setTampil] = useState([])
    useEffect(() => {
        (async () => {
            try {
                const response = await axios.get(`https://jsonplaceholder.typicode.com/todos`)
                const resData = await response.data
                let datane = resData.map(item => {
                    return { key: item.id, iduser: item.userId, judul: item.title, complete: item.completed }
                })
                setTampil(datane)
            } catch (e) {
                console.log(e)
            }
        })()
    }, [])
    return (
        <div>
            {
                dataTampil.map((item) => (
                    <div key={item.key}>
                        {item.key}{" "}{item.judul}
                    </div>
                ))
            }
        </div>
    )
}

export default Example
import { DeleteOutlined, EditOutlined, EyeOutlined, FileAddOutlined } from '@ant-design/icons';
import { Button, Table } from 'antd';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

const Home = () => {
    let navigate = useNavigate()
    let [dataAction, setDataAction] = useState([])
    const columns = [
        {
            title: 'Nama',
            dataIndex: 'nama',
            key: 'nama',
        },
        {
            title: 'Harga',
            dataIndex: 'harga',
            key: 'harga',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Action',
            key: 'action',
            render: (status, item) => (
                <>
                    <Button onClick={() => navigate(`/view/${item.key}`)}><EyeOutlined /></Button>{" "}
                    <Button onClick={() => navigate(`/edit/${item.key}`)}><EditOutlined /></Button>{" "}
                    <Button onClick={() => navigate(`/delete/${item.key}`)} type='danger'><DeleteOutlined /></Button>
                </>
            ),
        },
    ];

    useEffect(() => {
        (async () => {
            try {
                const response = await axios.get(`https://6257777f74007111adf7b5a8.mockapi.io/pakan`)
                const resData = await response.data
                let datane = resData.map(item => {
                    return { key: item.id, nama: item.nama_pakan, harga: item.harga_pakan, status: item.status_pakan }
                })
                setDataAction(datane)
            } catch (e) {
                console.log(e)
            }
        })()
    }, [])

    return (
        <>
            <h2>Data Tabel</h2>
            <Button onClick={() => navigate('/create')} type='primary'><FileAddOutlined /></Button>
            <Table columns={columns} dataSource={dataAction} size="small" bordered={true}
                pagination={{
                    pageSize: 5,
                }}
                scroll={{
                    x: 'auto',
                }} />
        </>
    )
}

export default Home
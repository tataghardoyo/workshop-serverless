import { Button, Form, Input, InputNumber, notification } from 'antd'
import React from 'react'
import axios from 'axios'

const Create = () => {
  const onFinish = (values) => {
    // console.log('Success:', values);
    axios.post('https://6257777f74007111adf7b5a8.mockapi.io/pakan', {
      nama_pakan: values.namapakan,
      harga_pakan: values.hargapakan,
      status_pakan: values.statuspakan
    })
      .then(response => {
        notification.open({
          message: 'Save Data',
          description: 'Success'
        });
      })
      .catch(error => {
        console.error(error);
      });
  };
  return (
    <div>
      <h2>Create</h2>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Nama"
          name="namapakan"
          rules={[
            {
              required: true,
              message: 'Please input your name!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Harga"
          name="hargapakan"
          rules={[
            {
              required: true,
              message: 'Please input your price!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          label="Status"
          name="statuspakan"
          rules={[
            {
              required: true,
              message: 'Please input your status!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Create
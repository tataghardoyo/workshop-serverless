import { Button, notification } from 'antd'
import React from 'react'
import axios from 'axios'
import { useParams, useNavigate } from 'react-router-dom'

const Delete = () => {
    const { id } = useParams()
    let navigate = useNavigate()
    const onDelete = () => {
        axios.delete('https://6257777f74007111adf7b5a8.mockapi.io/pakan/' + id)
            .then(response => {
                notification.open({
                    message: 'Delete Data',
                    description: 'Success'
                });
                navigate('/')
            })
            .catch(error => {
                console.error(error);
            });
    }
    return (
        <div>
            Delete {id}
            <h2>Delete this ?</h2>
            <Button type='danger' onClick={onDelete}>Delete</Button>{" "}
            <Button>Cancel</Button>
        </div>

    )
}

export default Delete
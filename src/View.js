import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios';
import { Input } from 'antd';

const View = () => {
    const { id } = useParams()
    const [nama, setNama] = useState()
    const [harga, setHarga] = useState()
    const [status, setStatus] = useState()

    useEffect(() => {
        (async () => {
            try {
                const response = await axios.get(`https://6257777f74007111adf7b5a8.mockapi.io/pakan/1${id}`)
                const resData = await response.data
                console.log(resData)
                setNama(resData.nama_pakan)
                setHarga(resData.harga_pakan)
                setStatus(resData.status_pakan)
            } catch (e) {
                console.log(e)
            }
        })()
    }, [id])

    return (
        <div>
            <h2>View {id}</h2>
            <Input style={{ marginBottom: '10px' }} value={nama} disabled />
            <Input style={{ marginBottom: '10px' }} value={harga} disabled />
            <Input style={{ marginBottom: '10px' }} value={status} disabled />
        </div>
    )
}

export default View
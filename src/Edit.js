import { ReloadOutlined } from '@ant-design/icons'
import { Form, Input, Button, InputNumber, notification } from 'antd'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const Edit = () => {
    const { id } = useParams()
    let [loading, setLoading] = useState(true)
    let [error, setError] = useState(false)

    const [nama, setNama] = useState()
    const [harga, setHarga] = useState()
    const [status, setStatus] = useState()

    useEffect(() => {
        (async () => {
            try {
                const response = await axios.get(`https://6257777f74007111adf7b5a8.mockapi.io/pakan/${id}`)
                const resData = await response.data
                console.log(resData)
                setNama(resData.nama_pakan)
                setHarga(resData.harga_pakan)
                setStatus(resData.status_pakan)
                setLoading(false)
            } catch (e) {
                console.log(e)
                setLoading(false)
                setError(true)
            }
        })()
    }, [id])

    const onFinish = (values) => {
        console.log('Success:', values);
        axios.put('https://6257777f74007111adf7b5a8.mockapi.io/pakan/' + id, {
            nama_pakan: values.namapakan,
            harga_pakan: values.hargapakan,
            status_pakan: values.statuspakan
        })
            .then(response => {
                notification.open({
                    message: 'Edit Data',
                    description: 'Success'
                });
            })
            .catch(error => {
                console.error(error);
            });
    };

    if (loading) {
        return (
            <>
                <Button onClick={() => window.location.reload()}><ReloadOutlined /></Button>
                Loading...
            </>
        )
    }
    if (error) {
        return (
            <>
                <div>Error, Please Try Again !!!</div>
                <Button onClick={() => window.location.reload()}><ReloadOutlined /></Button>
            </>
        )
    }

    return (
        <div>
            <h2>Edit {id}</h2>
            <Form
                name="basic"
                initialValues={{
                    namapakan: `${nama}`,
                    hargapakan: harga,
                    statuspakan: `${status}`
                }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.Item
                    label="Nama"
                    name="namapakan"
                    rules={[
                        {
                            required: true,
                            message: 'Please input name!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Harga"
                    name="hargapakan"
                    rules={[
                        {
                            required: true,
                            message: 'Please input harga!',
                        },
                    ]}
                >
                    <InputNumber />
                </Form.Item>

                <Form.Item
                    label="Status"
                    name="statuspakan"
                    rules={[
                        {
                            required: true,
                            message: 'Please input status!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>

        </div>
    )
}

export default Edit
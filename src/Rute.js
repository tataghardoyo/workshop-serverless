import React from 'react'
import { BrowserRouter, Outlet, Route, Routes, Link } from 'react-router-dom'
import { Breadcrumb, Layout } from 'antd';
import Home from './Home';
import Create from './Create';
import View from './View';
import Edit from './Edit';
import Delete from './Delete';
import Example from './Example';

const { Header, Content, Footer } = Layout;
const Rute = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Template />}>
                        <Route index element={<Home />} />
                        <Route path="create" element={<Create />} />
                        <Route path="view/:id" element={<View />} />
                        <Route path="edit/:id" element={<Edit />} />
                        <Route path="delete/:id" element={<Delete />} />
                        <Route path="contoh" element={<Example />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )
}

const Template = () => {
    return (
        <>
            <Layout className="layout">
                <Header>
                    <div className="logo" />
                     <Link to="/" style={{color:'white'}}>Home</Link>
                </Header>
                <Content
                    style={{
                        padding: '0 50px',
                    }}
                >
                    <Breadcrumb
                        style={{
                            margin: '16px 0',
                        }}
                    >
                        {/* <Breadcrumb.Item>Home</Breadcrumb.Item> */}
                    </Breadcrumb>
                    <div className="site-layout-content" style={{ minHeight: '500px' }}>
                        <Outlet />
                    </div>
                </Content>
                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Ant Design ©2018 Created by Ant UED
                </Footer>
            </Layout>
        </>
    )
}

export default Rute